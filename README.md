# This tiny script corrects your faults :)

## Usage
#### For files:
```Shell
python codestyle-objc.py [file] [file] ... [--safe]
```
#### For files of commit:
```Shell
python codestyle-objc.py [sha] [--safe]
```
> `--safe` : will NOT change any files, just count entries;

# SourceTree Custom Action
```Shell
Script to run: <Full path to the .py file>
```
Use corresponding actions for files while staging changes or for commits.
![screenshot](_imgs/custom-actions.png)

> Try setting `chmod 777` for the .py file if SourceTree is raising errors.