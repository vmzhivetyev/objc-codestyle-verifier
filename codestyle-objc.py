#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import codecs
import sys
import re

def indexOf(str, substr):
	try:
		return str.index(substr)
	except Exception as e:
		return -1

def fixMultilineMethodsTabulation(data):
	'''
	repairs indents in multiline methods signatures by ':' characters
	'''
	data = data.replace(u'\t',u'    ')

	strs = data.split(u'\n')
	last = 0
	for si in range(0, len(strs)):
		s = strs[si]
		
		if last > 0:
			nospace = s.strip()
			wordsBefore = indexOf(nospace,':')
			if wordsBefore < 1:
				last = 0
				continue

			s = u' '*(last - wordsBefore) + nospace
			strs[si] = s
			print('replacing ',last,'with',last - wordsBefore)

		if indexOf(s,';') > 0:
			last = 0
			continue

		last = indexOf(s,':')

	data = u'\n'.join(strs)
	return data

def removeUnusedForwardDeclarartions(data):
	rall = r'(@(?:class|protocol) (\w+);\n)'
	declarations = re.findall(rall, data, flags=re.MULTILINE)
	for dec in declarations:
		rname = dec[1] + r'\W'
		count = len(re.findall(rname, data, flags=re.MULTILINE))
		if count == 1:
			data = re.sub(re.escape(dec[0]), '', data, flags=re.MULTILINE)

	return data

def replace(rules, path, filename):
	global stats
	global totalKey
	global flagDontCount
	global flagSafe
	global flagOnlyHeader

	if not os.path.isfile(path):
		return

	with codecs.open(path, 'r', 'utf-8') as file:
		data = file.read()
		initialdata = data

		data = fixMultilineMethodsTabulation(data)
		data = removeUnusedForwardDeclarartions(data)

		for rule in rules:
			if flagOnlyHeader in rule and path[-2:] != '.h':
				continue

			old = rule[0]
			new = rule[1]
			comment = rule[2]

			print('Evaluating "' + str(old) + '"" -> "' + str(new) + '"')

			count = -1
			limit = 10
			while (count < 0) or (count > 0 and limit > 0):
				limit -= 1
				count = len(re.findall(old, data, flags=re.MULTILINE))
				data = re.sub(old, new, data, flags=re.MULTILINE)
		
				if stats is not None:
					filenameKey = filename

					if not filenameKey in stats:
						stats[filenameKey] = { totalKey : 0 }

					changeCountKey = comment if comment else 'other'

					if not changeCountKey in stats[filenameKey]:
						stats[filenameKey][changeCountKey] = 0

					if not flagDontCount in rule:
						stats[filenameKey][changeCountKey] += count
						stats[filenameKey][totalKey] += count
		print(str(stats[filename][totalKey]) + '\t' + filenameKey)

	if not flagSafe:
		if initialdata != data:
			print('Saving file '+filename)
			with codecs.open(path, 'w', 'utf-8') as file:
				file.write(data)
			
def renames(rules, files, ignore_hidden_files=True):
	for filename in files:
		path = filename

		extension = path.split('.')[-1]
		if extension not in ['h', 'm']:
			continue

		replace(rules, path, path.split('/')[-1])

def updateGit():
	import subprocess, os, sys
	pyfilepath = os.path.dirname(os.path.realpath(sys.argv[0])) 

	print('Doing "git pull" in '+pyfilepath)
	output = subprocess.check_output(["git", "pull"], cwd=pyfilepath)
	print(output)

if __name__ == "__main__":
	updateGit()

	folder = None
	totalKey = '= Total'
	flagDontCount = 'FLAG_DONT_COUNT'
	flagSafe = '--safe' in sys.argv
	flagOnlyHeader = 'only in .h files'

	if len(sys.argv) == 1:
		raise ValueError("\n\nUSAGE: [FILE] [FILE] ... [--safe];\n\n\t--safe : will NOT change any files, just count entries;")

	files = sys.argv[1:]
	if flagSafe:
		files.remove('--safe')

	if len(files) == 1 and not os.path.isfile(files[0]):
		# is it commit SHA?
		# lets get files list of the commit
		terminalCommandForFilesListOfCommit = 'git diff-tree --no-commit-id --name-only -r '+files[0]
		files = os.popen(terminalCommandForFilesListOfCommit).read().split('\n')

		# engage clang formatter
		useClangFormatter = False
		if useClangFormatter:
			clangBin = '/usr/local/bin/clang-format'
			if not os.path.isfile(clangBin):
				raise ValueError('Error: clang-format binary not found, install with "brew install clang-format".')
			else:
				os.popen(terminalCommandForFilesListOfCommit + ' | xargs '+clangBin+' -i -style=file')


	rules = [
		[r'([^\n])\n\/\*\*', 
		 r'\1\n\n/**', 
		'empty line before javadoc'],

		# *

		[r'([^\s*\/])\* ([^\s*])', 
		 r'\1 *\2', 
		'right position of *'],

		# copy

		[r'^(@property.*)strong(.*)(NSString|NSArray|NSSet|NSDictionary)(.*)$', 
		 r'\1copy\2\3\4', 
		'copy for potentialy mutable property'],

		# two lines before pragma

		[r'(\S)(?:\s{2}|\s{4,})(#pragma mark|@protocol|@interface|@implementation)', 
		 r'\1\n\n\n\2', 
		'two empty lines before "#pragma" "@rotocol" or "@interface" or "@implementation"'],

		[r'(\S)\s*(@end)',
		r'\1\n\n\2',
		'one empty line before @end'],

		# one line before imports

		[r'\/\/\n\n\n([@#]import)', 
		 r'//\n\n\1', 
		'one empty line before all imports',
		flagDontCount],
		[r'___FILEHEADER___\n\n\n([@#]import)', 
		 r'___FILEHEADER___\n\n\1', 
		'one empty line before all imports',
		flagDontCount],

		# [r'(NSArray|NSSet) ?\*', 
		#  r'\1<id> *', 
		# 'generic for NSSet and NSArray'],

		# [r'(NSDictionary) ?\*', 
		#  r'\1<id, id> *', 
		# 'generic for NSDictionary'],

		# javadoc for property

		[r'^\/\*\*\s+(.*)\s+\*\/\n(@property.*;)', 
		 r'\2 /**< \1 */', 
		'javadoc format for @property'],

		# spaces->tabs

		[(' '*4), 
		 r'\t', 
		'tabs instead of spaces',
		flagDontCount],

		# excessive nonnull

		[r'(NS_ASSUME_NONNULL_BEGIN[\S\s]*)nonnull([\S\s]*NS_ASSUME_NONNULL_END)', 
		 r'\1<#NS_ASSUME_NONNULL_BEGIN SPECIFIED#>\2', 
		'nonnull inside NS_ASSUME_NONNULL_BEGIN'],

		# spaces

		[r'(for|if)\(', 
		 r'\1 (', 
		'spaces in for_() if_()'],

		# @import

		[r'#import <Foundation/Foundation\.h>', 
		 r'@import Foundation;', 
		'@import Foundation;'],

		[r'#import <UIKit/UIKit\.h>', 
		 r'@import UIKit;', 
		'@import UIKit;'],

		[r'#import <XCTest/XCTest\.h>', 
		 r'@import XCTest;', 
		'@import XCTest;'],

		[r'#import <OCMock/OCMock\.h>', 
		 r'@import OCMock;', 
		'@import OCMock;'],

		[r'#import <Expecta/Expecta\.h>', 
		 r'@import Expecta;', 
		'@import Expecta;'],

		[r'\[0\]', 
		 r'.firstObject', 
		'.firstObject instead of [0]'],

		# missing javadocs

		[r'(@property|extern)(.*;)\s*($)', 
		 r'\1\2 /**< <#JAVADOC#>. */\3\n', 
		'missing javadoc for properties and constants',
		flagOnlyHeader],

		# wrong format of javadoc for method

		# [r'((?:-|\+).*;).*?\/\*\*<? ?(.*?) *\*\/', 
		#  r'<#\2#>\n\1', 
		# 'wrong format of javadoc for method',
		# flagOnlyHeader],
		
		# missing javadoc for method
		[r'([^\/])\n((?:-|\+))', 
		 r'\1\n/** <#JAVADOC#> */\n\2', 
		'missing javadoc for method',
		flagOnlyHeader],

		#### evaluate at the end  ####
		# missing newline

		[r'(.+)\Z', 
		 r'\1\n', 
		'missing newline at end of file'],

		# lines between methods
		[r'(})\s+((?:-|\+) \()',
		r'\1\n\n\2',
		'one empty line between methods'],

		[r'(#pragma mark - .*)\n+((?:-|\+) \()',
		r'\1\n\n\2',
		'one empty line after #pragma mark - ...'],

		[r'(\(nonatomic, )(?:nonnull, )?(weak)(.*)(?:, nullable)(.*\))',
		r'\1nullable, \2\3\4',
		'...nullable, weak...']


	]
	print('\n\n============== evaluating %i rules =================' % (len(rules)) );
	print('Working files: "%s"' % '\n'.join(files))
	stats = {}
	
	renames(rules, files)

	summary = { totalKey : 0 }

	for fileKey in stats:
		if stats[fileKey][totalKey] == 0:
			continue

		print('\n'+fileKey)
		print('%s\t%s' % (str(stats[fileKey][totalKey]), totalKey))
		for ruleKey in stats[fileKey]:
			if ruleKey != totalKey:
				print('%s\t%s' % (str(stats[fileKey][ruleKey]), ruleKey))

			if not ruleKey in summary:
				summary[ruleKey] = 0
			summary[ruleKey] += stats[fileKey][ruleKey]

	print('\nSummary for %i files:' % len(stats))
	print('%s\t%s' % (str(summary[totalKey]), totalKey))
	for ruleKey in summary:
		if ruleKey != totalKey:
			print('%s\t%s' % (str(summary[ruleKey]), ruleKey))


	print('\n======== DONE ========\n\n');

















